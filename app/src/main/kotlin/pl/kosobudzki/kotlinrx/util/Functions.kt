package pl.kosobudzki.kotlinrx.util

import android.util.Log

/**
 * @author krzysztof.kosobudzki
 */
public fun String.log() {
    Log.d("kotlinrxtest", this)
}