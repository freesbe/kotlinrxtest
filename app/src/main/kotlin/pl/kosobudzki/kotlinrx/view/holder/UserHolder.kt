package pl.kosobudzki.kotlinrx.view.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import pl.kosobudzki.kotlinrx.R
import pl.kosobudzki.kotlinrx.model.User
import butterknife.bindView
import android.widget.ImageView
import com.squareup.picasso.Picasso
import android.widget.Button
import pl.kosobudzki.kotlinrx.util.log

/**
 * @author krzysztof.kosobudzki
 */
public class UserHolder(val userView : View) : RecyclerView.ViewHolder(userView) {
    val mLoginTextView : TextView by bindView(R.id.user_login_text_view)
    val mProfileImageView : ImageView by bindView(R.id.user_image_view)
    val mCloseButton : Button by bindView(R.id.close_button)
    
    fun bind(user : User) {
        mLoginTextView.setText(user.login)
        mCloseButton.setOnClickListener { "OnClick ${user.login}".log() }
        
        Picasso.with(userView.getContext())
                .load(user.avatarUrl)
                .into(mProfileImageView)
    }
}