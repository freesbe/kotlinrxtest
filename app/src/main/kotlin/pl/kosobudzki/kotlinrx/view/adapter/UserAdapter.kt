package pl.kosobudzki.kotlinrx.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import pl.kosobudzki.kotlinrx.view.holder.UserHolder
import pl.kosobudzki.kotlinrx.model.User
import android.view.View
import android.view.LayoutInflater
import pl.kosobudzki.kotlinrx.R

/**
 * @author krzysztof.kosobudzki
 */
public class UserAdapter : RecyclerView.Adapter<UserHolder>() {
    val mUsers = arrayListOf<User>()
    
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): UserHolder? {
        val view : View = LayoutInflater.from(parent?.getContext()).inflate(R.layout.item_view, parent, false)
        
        return UserHolder(view)
    }

    override fun onBindViewHolder(holder: UserHolder?, position: Int) {
        holder?.bind(mUsers.get(position))
    }

    override fun getItemCount(): Int {
        return mUsers.size()
    }
    
    fun addUser(user : User) {
        mUsers.add(user)
    }
}