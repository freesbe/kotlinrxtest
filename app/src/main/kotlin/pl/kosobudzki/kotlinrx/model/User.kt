package pl.kosobudzki.kotlinrx.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonIgnoreProperties

/**
 * @author krzysztof.kosobudzki
 */
[JsonIgnoreProperties(ignoreUnknown = true)]
data class User(
        val id : Int, 
        val login : String, 
        [JsonProperty("avatar_url")]
        val avatarUrl : String
)