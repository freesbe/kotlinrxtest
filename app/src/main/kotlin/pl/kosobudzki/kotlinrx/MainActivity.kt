package pl.kosobudzki.kotlinrx

import android.support.v7.app.ActionBarActivity
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import butterknife.bindView
import android.support.v7.widget.LinearLayoutManager
import pl.kosobudzki.kotlinrx.view.adapter.UserAdapter
import rx.Observable
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.Request
import rx.schedulers.Schedulers
import rx.android.schedulers.AndroidSchedulers
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import pl.kosobudzki.kotlinrx.model.User
import pl.kosobudzki.kotlinrx.util.log
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.JavaType
import pl.kosobudzki.kotlinrx.view.holder.UserHolder

/**
 * @author krzysztof.kosobudzki
 */
public class MainActivity : ActionBarActivity() {
    val mRecyclerView : RecyclerView by bindView(R.id.users_recyler_view)
    
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        
        setContentView(R.layout.activity_main)
        
        mRecyclerView.setLayoutManager(LinearLayoutManager(this))
        mRecyclerView.setAdapter(UserAdapter())
        
        val client = OkHttpClient()
        val mapper = jacksonObjectMapper()
        
        Observable.just(getString(R.string.api_url))
            .map { url -> Request.Builder().url(url).build() }
            .map { request -> client.newCall(request).execute() }
            .map { response -> response.body().string() }
            .map { (json) : List<User> ->  mapper.readValue(
                    json, 
                    mapper.getTypeFactory().constructCollectionType(javaClass<List<User>>(), javaClass<User>())
            ) }
            .flatMap { list -> Observable.from(list) }
            .take(15)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { user -> run { 
                (mRecyclerView.getAdapter() as UserAdapter).addUser(user)
                
                mRecyclerView.getAdapter().notifyDataSetChanged()
            } }
    }
}